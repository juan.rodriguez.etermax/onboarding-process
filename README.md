# Welcome to Etermax
This document will guide you through the **onboarding** process

Before starting, you can check the slack channels of interest [here](http://confluence.etermax.com/confluence/display/ingenieria/Slack+channels). And [here](http://confluence.etermax.com/confluence/display/ingenieria/Roles) are the descriptions and responsibilities of the different roles you will find in the Etermax Engineering Team.

Also, stay tuned for your emails and your calendar. HR will surely send you important information that you should read as soon as possible.

---

##### NOTES:
* The `📅 Day` indicates the average day in which other new hirees before you have completed the tasks. 
* The `onboarding buddy` is a software engineer who will guide you across the process.
Ask your manager if you have no one assigned yet. 

---

##### 📅 Day 1
## Git introduction (Optional)
 * https://try.github.io
 * http://pcottle.github.io/learnGitBranching/ (Keep in mind there are two tabs: local and remote).

## Clean Code
[Uncle Bob](https://twitter.com/unclebobmartin) defined what good code should look like in his book **Clean Code**. Here at Etermax, we follow these practices on a daily basis.

Watch and discuss the following videos (you can skip astronomical introductions of each video):
* [E01 - Clean Code](https://drive.google.com/file/d/1eMuWwjHKyG1FHVPyK8cRBvA0VVIuyAo3/view?usp=sharing) | Subs: [ES](uploads/121c9e6ed6adac6319f4a58ce6860d40/Clean_Code_E01_-_Clean_Code.srt) :flag_es: 
* [E02 - Names](https://drive.google.com/file/d/1meivrylGI_Cgu1r1X_eFVKTt25y6eerA/view?usp=sharing) | Subs: [PT](uploads/86ffdbfecfc707f083b6075fdea95924/Clean_Code_E02_-_Names.srt) :flag_br:
* [E03 - Functions](https://drive.google.com/file/d/1bdzrq1aiab3M7tZImWXo3mFhHIXl5I0m/view?usp=sharing)
* [E04 - Function Structure](https://drive.google.com/file/d/1f9dcvPiyMor-lu33UCDpc2dZc9sCB1Bo/view?usp=sharing)

You can get all the videos [here](https://drive.google.com/drive/folders/19CtqCVHwkiLYRXC9Qg-D7OV1kPx0RWQx) (login with your Etermax credentials)

### [KATA] The goose game rules: Iteration 1 - Apply Clean Code
Now let's write some code, start the [iteration 1 of the goose game rules kata](https://gitlab.com/juan.rodriguez.etermax/onboarding-process/-/wikis/goose1) applying Clean Code.

##### 📅 Day 2
## Test Driven Development
Test-Driven Development (TDD) is a technique for building software that guides software development by writing tests. We use TDD in all our projects.

Watch and discuss the following videos:
*  [E06 - TDD Part 1](https://drive.google.com/file/d/1C9CfFSU1djyRXN6_cfN_-IsUxaAJvgmY/view?usp=sharing)
*  [E06 - TDD Part 2](https://drive.google.com/file/d/1sm1rq9EzZUPgtYlto7YOHaZLUJxA-KBs/view?usp=sharing)
*  [E19 - Advanced TDD - Part 1](https://drive.google.com/file/d/1wMnRhIDFYDfN-x3F4YXWP9yg6uViFIl4/view?usp=sharing)
*  [E19 - Advanced TDD - Part 2](https://drive.google.com/file/d/1cDkhHMWosUxiHSG7Ld_8raCFbVUofOpN/view?usp=sharing)

### [KATA] The goose game rules: Iteration 2 - Apply TDD
Now complete the [iteration 2 of the goose game rules kata](https://gitlab.com/juan.rodriguez.etermax/onboarding-process/-/wikis/goose2) applying TDD.
Remember the red-green-refactor cycle and stay red as little as possible.

##### 📅 Days 2 to 3
## SOLID, Clean Architecture
Like Clean Code and TDD, SOLID and Clean Architecture are practices that are followed on Etermax on a daily basis.

Watch and discuss the following videos:
* [E07 - Architecture, Use Cases, and High-Level Design](https://drive.google.com/file/d/1hrkyuRcqbbhDJfCuKMvmakrgcKb9KwRC/view?usp=sharing)
* [E08 - Foundations of the SOLID principles](https://drive.google.com/file/d/10dJr0HmVPkzLZIaARNiMLFMaThX1sIQU/view?usp=sharing) | Subs: [ES](uploads/7f8030767ea4b441a7f7cb1ccbbfe591/Clean_Code_E08_-_Foundations_of_the_SOLID_principles.srt) :flag_es:, [EN](uploads/2f0c0339648f6a8981e1fa7f2d453cda/Clean_Code_E08_-_Foundations_of_the_SOLID_principles.srt) :flag_gb: 
* Ask your onboarding buddy about the [SOLID talk](https://docs.google.com/presentation/d/1jTovjz6dtikVqXgcAvEtA402fVBmyfovDqc1sY-kluU/edit?usp=sharing)(last recorded session [here](https://drive.google.com/file/d/1NMpt_FI2gcfYSkXG0IqGK761eRq1o04o/view?usp=sharing))

### [KATA] The goose game rules: Iteration 3 - Apply SOLID
Discuss the solution with your onboarding buddy, talk about SOLID principles and refactor your solution.

When the refactoring is done, add the rules in the [iteration 3 of the goose game rules kata](https://gitlab.com/juan.rodriguez.etermax/onboarding-process/-/wikis/goose3) without modifying the existing code.

##### 📅 Days 4 to 10
## Interaction-Driven Design

**IDD** is an iterative approach to software design and development based on **Outside-In Development** proposed by [Sandro Mancuso](https://twitter.com/sandromancuso). This approach focus on modelling behaviour according to the external usage of the system while maintaining an internal representation of cohesive business components.

 * [Outside-In Development](https://codurance.com/2017/10/23/outside-in-design/)
 * [Interaction-Driven Design introduction](https://codurance.com/2017/12/08/introducing-idd/)
 * [IDD talk: Sandro Mancuso](https://codurance.com/videos/2015-04-24-interaction-driven-design/) and [slides](https://www.slideshare.net/sandromancuso/crafted-design-itake-2014)
 * Comparative Case Study (first two videos [here](https://drive.google.com/drive/u/1/folders/1yo-lSK4BeOw7sVtwl9YKpZHylEFqxsms). Watch Sandro Mancuso apply IDD alongside Uncle Bob.
 * Ask your onboarding buddy about the [IDD talk](https://docs.google.com/presentation/d/1-py4f41xl9aBvUcs5S7kGcwFbnbzwv6YGf9KThXJUOo/edit?usp=sharing): Main components are explained using a productive example in Preguntados.

##### 📅 Day 11
## Client architecture (Model View Presenter, Model View ViewModel)
Read [this article about MVP](https://android.jlelse.eu/architectural-guidelines-to-follow-for-mvp-pattern-in-android-2374848a0157)
and [this one about MVVM](https://wojciechkulik.pl/ios/mvvm-coordinators-rxswift-and-sample-ios-application-with-authentication) to discuss with your onboarding buddy.

##### 📅 Day 12
# Continuous Integration
Read and discuss with your onboarding buddy the Martin's Fowler article about [Continuous Integration](https://www.martinfowler.com/articles/continuousIntegration.html)

# Docker
Read parts 1 and 2 (orientation, containers) of the [Docker introduction](https://docs.docker.com/get-started/) and discuss with your onboarding buddy the benefits of containerization.

# Product introduction
Arrange with the Product Owner (PO) of your team the 
[presentation](https://docs.google.com/presentation/d/1rmgvluiAfvVtDChXo0Xrk3ehLsFLinx7-Dgx6Zv0ARU/edit?usp=sharing) about the product.
If you want, you can watch [this talk](https://drive.google.com/drive/u/1/folders/1Un7YnR2jfIYYz5HTaW3HCrAQPkR_BNUK) for more info.

## Welcome aboard! :ship: 
You can start with these tasks:
* Ask permissions for NewRelic, Firebase, Amplitude, and whatever tools the team uses frequently.
* Clone the main projects for your team, and ask for an introduction to their architecture.
